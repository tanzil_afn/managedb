"use strict";
//dependencies
var http = require('http');
var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var session = require('express-session')

var app = express();

//session manage
app.use(session({secret: 'keyboard cat'}))

//routing variables
var indexRoute = require(path.join(process.cwd(), 'routes', 'index.js'));
var SQLITE = require(path.join(process.cwd(), 'routes', 'SQLITE.js'));
var SQLITEtable = require(path.join(process.cwd(), 'routes', 'SQLITEtable.js'));
var CreateTable = require(path.join(process.cwd(), 'routes', 'CreateTable.js'));
var DeleteRow = require(path.join(process.cwd(), 'routes', 'deleteRow.js'));
var EditRow = require(path.join(process.cwd(),'routes', 'editRow.js'))



//stormpath api file path
var options = {
  host: 'localhost',
  port: 2323
};

//check if server is already running
http.get(options, function(res) {
  console.log('server is running, redirecting to localhost');
  if (window.location.href.indexOf('localhost') < 0) {
    window.location = 'http://localhost:' + app.get('port');
  }
}).on('error', function(e) {
  // all environments
  app.set('port', process.env.PORT || 2323);
  app.set('views', process.cwd() + '/views');
  app.set('view engine', 'ejs');
  app.use(require('stylus').middleware(path.join(process.cwd(), 'public')));
  app.use(express.static(path.join(process.cwd(), 'public')));

  //body-parser
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: false}));


  //routing
  app.get('/', indexRoute.index);
  app.get('/test', indexRoute.test);
  app.get('/SQLITE',SQLITE.connectGet);
  app.post('/SQLITE', SQLITE.connectPost);
  app.get('/SQLITEtable/:name', SQLITEtable.viewTableGet);
  app.get('/CreateTable',CreateTable.createGet);
  app.post('/CreateTable',CreateTable.createPost);
  app.get('/deleteRow',DeleteRow.deleteRowGet);
  app.get('/editRow',EditRow.editRowGet);
  app.post('/editRow',EditRow.editRowPost);


  //app.on('stormpath.ready', function() {
    http.createServer(app).listen(app.get('port'), function (err) {
      console.log('server created');
      if (window.location.href.indexOf('localhost') < 0) {
        window.location = 'http://localhost:' + app.get('port');
      }
    });
  //});
});
