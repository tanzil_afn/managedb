/**
 *  \copyright      Agdoom
 */
exports.agdm = ( function( _agdm )
{
    // namespace / module
    _agdm.db = ( function( _db )
    {
        // namespace / module
        _db.sqlite3 = ( function( _sqlite3 )
        {
            /**
             *  Core
             */
            _sqlite3.Core = function()
            {
                var _api = require( 'sqlite3' ).verbose();
                var _conn = null;

                /// \todo Check if _api is loaded.
                if( !_api )
                {
                    // do something later ...
                }
                
                /**
              ta   *  Initialises. Opens database, if doesnot exists, creates a new one.
                 * 
                 * \param[out]      func_succ       callback( msg )
                 * \param[out]      func_fail       callback( msg, err )
                 * \param[in]       info            { name: '', mode: '' }
                 */
                this.Init = function(info, func_succ, func_fail)
                {
                    if( undefined === info.mode )
                        info.mode = [];

                    /*
                    _conn = new _api.Database( info.name, info.mode, function(err){
                            if(err)
                            {
                                var err_msg = 'agdm.db.sqlite3.Core.Init: Failed to open ' + info.name;
                                console.log( err_msg );
                                func_fail( msg, err );
                                return;
                            }

                            alert( 'successful' );

                            console.log( 'agdm.db.sqlite3.Core.Init: Opened ' + info.name );
                            func_succ( 'established.' );
                        } );
                    */

                    _conn = new _api.Database( info);

                    /** \note   Need to use callback with _api.Database => ( info.name, info.mode, callback )*/

                    if( _conn )
                    {
                        console.log( 'agdm.db.sqlite3.Core.Init: Opened ' + info.name );
                        func_succ( 'established.' );
                    }
                    else
                    {
                        var err_msg = 'agdm.db.sqlite3.Core.Init: Failed to open ' + info.name;
                        console.log( err_msg );
                        func_fail( msg, err );
                    }
                }   // Init
                
                /**
                 * \param[out]      func_succ       callback. def: func_succ( rows )
                 * \param[out]      func_fail       callback. def: func_fail( msg, err )
                 * \param[in]       query           string. contains a fully formed sqlite3 query.
                 */
                this.Query = function( func_succ, func_fail, query )
                {
                    //console.log( 'query: ' + query );

                    _conn.all( query, [], function( err, rows ){
                            if( err )
                            {
                                var err_msg = 'agdm.db.sqlite3.Core.Query: Failed to query: ' + query;
                                console.log( err_msg );
                                func_fail( err_msg, err );
                                return ;
                            }   // if err

                            // for dbg ...
                            //console.log( 'agdm.db.sqlite3.Core.Query: Complete.' );
                            //console.log( rows );

                            func_succ( rows );
                        });
                }   // Query

                /**
                 *  This function returns a list of all the tables in the database.
                 *
                 * \param[out]      func_succ       callback. def: func_succ( table_list )
                 * \param[out]      func_fail       callback. def: func_fail( msg, err )
                 */
                this.List = function( on_succ, on_fail )
                {
                    //_conn.serialize( function(){
                            var str_query = 'select name from sqlite_master where type="table"';

                            _conn.all( str_query, function( err, tbl_list ){
                                    if( err )
                                    {
                                        var err_msg = 'agdm.db.sqlite3.Core.List: Failed to query: ' + str_query;
                                        console.log( err_msg );
                                        on_fail( err_msg, err );
                                        return ;
                                    }

                                    on_succ( tbl_list );
                                } );    // all
                                //// } );   // serialise
                }
            }   // class Core
            
            /**
             * \param[in]       name        string
             * \param[in]       type        string              (e.g. TEXT)
             * \param[in]       mod         array of string     (e.g. ['PRIMARY KEY', 'NOT NULL'])
             */
            _sqlite3.Field = function( name_, type_, mod_ )
            {
                this.name = name_;
                this.type = type_;
                this.mod = mod_;            //Primary or Unique or ...
            }   // class Field
            
            /**
             * Returns whether this field is Primary / Unique or not.
             */
            _sqlite3.Field.prototype.bPrimary = function()
            {
                return this.mod.some( function( item ){
                        return ('PRIMARY KEY' === item || 'UNIQUE' === item);
                    } );
            }
            
            /**
             * Join the members of Field to generate part of the query string.
             */
            _sqlite3.Field.prototype.Str = function()
            {
                var str_mod = this.mod.join( ' ' );
                
                if( str_mod.length )
                    return this.name + ' ' + this.type + ' ' + str_mod
                else
                    return this.name + ' ' + this.type;
            }
            
            /**
             * Class Table
             */
            _sqlite3.Table = function( tbl_name, core_obj )
            {
                var _name = tbl_name;
                var _core = core_obj;
                
                var _fields = [];   // Columns in a table.

                /**
                 * Returns core.
                 */
                this.Core = function()
                {
                    return _core;
                }

                /**
                 * This function loads table info.
                 */
                this.Info = function( on_succ, on_fail )
                {
                    var str_query = 'pragma table_info ( ' + _name + ' )';

                    _core.Query( on_succ, on_fail, str_query );
                }
                
                /**
                 * Create a new table.
                 * 
                 * \param[out]      func_succ       callback( rows )
                 * \param[out]      func_fail       callback( msg, err )
                 * \param[in]       arr_fields      array of sqlite3.Field
                 *                                  ( e.g. new [ new Field(), new Field(), new Field() ] )
                 */
                this.Create = function( func_succ, func_fail, arr_fields )
                {
                    // Check / Verify arr_fields
                    if( true )
                    {
                        /*
                        var cnt_primary = 0;
                        
                        arr_fields.map( function( item ){
                                if( item.bPrimary() )
                                    ++cnt_primary;
                            } );
                        */
                        var cnt_primary = arr_fields.reduce( function(a, b){
                                if( b.bPrimary() )
                                    return a+1;
                                else
                                   return a;
                            }, 0 );
                            
                        if( 1 < cnt_primary )
                        {
                            func_fail( 'agdm.db.sqlite3.Table.Create: More than one primary key provided' );
                            return ;
                        }
                    }   // if true
                    
                    // assign to _fields.
                    _fields = arr_fields;
                    
                    var arr_str_fields = [];
                    
                    _fields.map( function( item ){
                            arr_str_fields.splice( arr_str_fields.length, 0, item.Str() );
                        } );

                    var str_query = 'CREATE TABLE IF NOT EXISTS ' + _name + ' (' + arr_str_fields.join() + ')';
                    
                    // Call query in _conn
                    _core.Query( func_succ, func_fail, str_query );
                }

                /**
                 *  This function
                 */
                this.Open = function( func_succ, func_fail )
                {
                    //
                }

                /**
                 * INSERT INTO TABLE_NAME (column1, column2, column3,...columnN)]
                 *      VALUES (value1, value2, value3,...valueN);
                 * 
                 * str_ins = (column1, column2, column3,...columnN)] VALUES (value1, value2, value3,...valueN)
                 */
                this.Insert1 = function( func_succ, func_fail, str_ins )
                {
                    _core.Query( func_succ, func_fail, 'INSERT INTO ' + _name + ' ' + str_ins + ';' );
                }
                
                /**
                 * INSERT INTO TABLE_NAME (column1, column2, column3,...columnN)]
                 *      VALUES (value1, value2, value3,...valueN);
                 * 
                 * str_cols = column1, column2, column3,...columnN
                 * str_vals = value1, value2, value3,...valueN
                 */
                this.Insert2 = function( func_succ, func_fail, cols, vals )
                {
                    var str_cols = '';
                    var str_vals = '';

                    // Preprocess cols
                    if( Array.isArray(cols) )
                        str_cols = cols.join();
                    else if( 'string' === typeof cols )
                        str_cols = cols;
                    else
                    {
                        func_fail( 'agdm.db.sqlite3.Table.Insert2: "cols" with unsupported type.' );
                        return ;
                    }

                    // Preprocess vals
                    if( Array.isArray(vals) )
                        str_vals = vals.join();
                    else if( 'string' === typeof vals )
                        str_vals = vals;
                    else
                    {
                        func_fail( 'agdm.db.sqlite3.Table.Insert2: "vals" with unsupported type.' );
                        return ;
                    }
                    
                    // Call Instert1
                    this.Insert1( func_succ, func_fail, '(' + str_cols + ')' + 'VALUES' + '(' + str_vals + ')' );
                }
                
                
                /**
                 * INSERT INTO TABLE_NAME VALUES (value1,value2,value3,...valueN);
                 * 
                 * vals = value1,value2,value3,...valueN
                 */
                this.Insert3 = function ( func_succ, func_fail, vals)
                {
                    var str_vals = '';
                   
                    // Preprocess vals
                    if( Array.isArray(vals) )
                        str_vals = vals.join();
                    else if( 'string' === typeof vals )
                        str_vals = vals;
                    else
                    {
                        func_fail( 'agdm.db.sqlite3.Table.Insert2: "vals" with unsupported type.' );
                        return ;
                    }

                    // Call Instert1
                    this.Insert1( func_succ, func_fail, 'VALUES' + ' (' + str_vals + ')' );
                }
                
                /**
                 * SELECT * FROM table_name;
                 * SELECT column1, column2, columnN FROM table_name;
                 */
                this.Select1 = function( func_succ, func_fail, str_sel, str_wh )
                { 
                    if( str_wh )
                        _core.Query(
                                /*func_succ*/function(rows){
                                    // dbg
                                    //console.log( 'agdm.db.sqlite3.Table.Select1: Successful.' );
                                    //console.log( rows );

                                    //
                                var query = "pragma table_info( "+_name+" )";

                                //running query to get the collumn names of the table
                                _core.Query(function(colm){
                                        console.log("Collumn names found ... ... ...");
                                        console.log("Collumns Found... ... ...");
                                        var colmNames = [];
                                        colm.forEach(function(collumn){
                                            colmNames.push(collumn.name);
                                        });
                                        //res.send(colmNames);
                                        func_succ(rows, colmNames);

                                    },
                                    function(err){
                                        err = "Error Error Error ... ... ... ";
                                        func_fail(eer);

                                    },
                                    query
                                );

                            }
                            ,   /*func_fail*/function(msg, err){
                                    console.log( 'agdm.db.sqlite3.Table.Select1: Failed.' );
                                    func_fail( msg, err );
                                }
                            ,   'SELECT ' + str_sel + ' FROM ' + _name
                                + ' WHERE ' + str_wh
                                + ';'
                            );
                    else
                        _core.Query(
                                /*func_succ*/function(rows){
                                    //console.log( 'agdm.db.sqlite3.Table.Select1: Successful.' );
                                var query = "pragma table_info( "+_name+" )";

                                //running query to get the collumn names of the table
                                _core.Query(function(colm){
                                        console.log("Collumn names found ... ... ...");
                                        console.log("Collumns Found... ... ...");
                                        var colmNames = [];
                                        colm.forEach(function(collumn){
                                            colmNames.push(collumn.name);
                                        });
                                        //res.send(colmNames);
                                        func_succ(rows, colmNames);

                                    },
                                    function(err){
                                        err = "Error Error Error ... ... ... ";
                                        func_fail(eer);

                                    },
                                    query
                                );

                            }
                            ,   /*func_fail*/function(msg, err){
                                    console.log( 'agdm.db.sqlite3.Table.Select1: Failed.' );
                                    func_fail( msg, err );
                                }
                            ,   'SELECT ' + str_sel + ' FROM ' + _name
                                + ';'
                            );
                }
                
                /**
                 * SELECT * FROM table_name;
                 * SELECT column1, column2, columnN FROM table_name;
                 * 
                 * \param[in]       cols        array of strings
                 */
                this.Select2 = function( func_succ, func_fail, cols )
                {
                    var str_sel = '';

                    if( cols.length )
                        str_sel = cols.join();
                    else
                        str_sel = '*';

                    // call Select1
                    this.Select1( func_succ, func_fail, str_sel );
                }


                /**
                 * Special Select functionthat gets the collumn names
                 * along with the rows in the table.
                 */
                this.Select3 = function(func_succ, func_fail)
                {
                    var str_sl = '*';

                    this.Select1(
                        function(rows){
                            console.log("Rows Found ... ... ...")
                            var query = "pragma table_info( "+_name+" )";

                            //running query to get the collumn names of the table
                            _core.Query(function(colm){
                                    console.log("Collumn names found ... ... ...");
                                    console.log("Collumns Found... ... ...");
                                    var colmNames = [];
                                    colm.forEach(function(collumn){
                                        colmNames.push(collumn.name);
                                    });
                                    //res.send(colmNames);
                                    func_succ(rows, colmNames);

                            },
                            function(err){
                                err = "Error Error Error ... ... ... ";
                                func_fail(eer);

                            },
                            query
                            );

                        },
                        function(err){
                            err = "Error Error ... ...";
                            func_fail(err);
                        },
                        str_sl);

                }


                
                /**
                 *  @param[in]  {string}   str_upd
                 */
                this.Update1 = function( func_succ, func_fail, str_upd, str_wh )
                {
                    if( !str_wh )
                    {
                        func_fail( 'agdm.db.sqlite3.Table.Update1: No WHERE condition provided.' );
                        return ;
                    }

                    var str_query = 'UPDATE ' + _name
                            + ' SET ' + str_upd
                            + ' WHERE ' + str_wh
                            + ';'
                            ;

                    console.log( str_query );

                    _core.Query( func_succ, func_fail
                        ,   str_query
                        );
                }
                
                /**
                 *  DELETE FROM table_name WHERE [condition];
                 */
                this.Delete1 = function( func_succ, func_fail, str_cond )
                {
                    var str_where = '';
                    
                    if( str_cond.length )
                        str_where = 'DELETE FROM ' + _name + ' WHERE ' + str_cond + ';'
                    else
                        str_where = 'DELETE FROM ' + _name + ';';
                    
                    _core.Query( func_succ, func_fail, str_where );
                }
            }   // class table
            
            return _sqlite3;
        }) ( _db.sqlite3 || {} );

        return _db;
    }( _agdm.db || {} ) );

    return _agdm;
} ( exports.agdm || {} ) );
