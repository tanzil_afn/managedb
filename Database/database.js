exports.database = (function(_database)
{
	_database.sqlite3 = (function(_sqlite3)
	{
		_sqlite3.core = function()
		{
			var _api = require('sqlite3').verbose();

			this.connectDB = function(DB , func_succ , func_fail)
			{
				var db = new _api.Database(DB);
				db.serialize(function(){
					var query = "select name from   where type='table'";
					db.all(query,
						function(err , tables){
							if(err) {
								console.log("Error While Accessing Tables !!!!!");
								console.log(err);
							}
							else{
								console.log("Tables Found....");
								func_succ(tables);
								//res.render('SQLITEtable',{title: 'Home', tables: tables, data: [], colm:[] });
								//res.send(tables);
							}
						}
					);
				});
			}

			this.selectAllDB = function(DB,table, func_succ, func_fail)
			{
				var tableName = table;
				var db = new _api.Database(DB);
				var query = "select * from "+ tableName;
				db.all(query,
					function(err , rows){
						if(err) {
							console.log("Error While Accessing Rows !!!!!");
							console.log(err);
						}
						else{
							console.log("Rows Found....");

							db.all("pragma table_info( "+tableName+" )",
								function(err, colm)
								{
									if(err){
										console.log("Error while Accessing the collumns!!!!!");
										console.log(err);
									}
									else{
										console.log("Collumns Found... ... ...");
										var colmNames = [];
										colm.forEach(function(collumn){
											colmNames.push(collumn.name);
										});
										//res.send(colmNames);
										func_succ(rows, colmNames);

									}

								}
							);

							//res.render('SQLITEtable',{title: 'Home', data: rows, tables:[] });
							//res.send(rows);
						}
					}
				);
			}
		}
		return _sqlite3;
	}(_database.sqlite3 || {}))

	return _database;
}(exports.database || {}))

//sqlite3.Init(
//	databaseName,
//	function(msg){
//		console.log(msg);
//		sqlite3.List(
//			function(tables){
//				console.log('Tables Found');
//				res.render('SQLITEtable',{title: 'Home', tables: tables, data: [], colm:[]});
//			},
//			function(err){
//				console.log(err);
//			}
//		);
//	},
//	function(msg, err){
//		console.log(msg);
//		console.log(err);
//	}
//);
