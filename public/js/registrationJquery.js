$(document).ready(function(){
    //Kin Section Start
    $("#kinMemberBlock").hide();
    $("#kinNotMemberBlock").hide();
    $("#kinMember").click(function(){
        $("#kinMemberBlock").fadeIn(500);
        $("#kinNotMemberBlock").fadeOut();
        $("#k2").removeAttr("required");
        $("#k3").removeAttr("required");
        $("#k4").removeAttr("required");
        $("#k5").removeAttr("required");
        $("#k1").attr("required");

    });
    $("#kinNotMember").click(function(){
        $("#kinMemberBlock").fadeOut();
        $("#kinNotMemberBlock").fadeIn(500);
        $("#k1").removeAttr("required");
        $("#k2").attr("required");
        $("#k3").attr("required");
        $("#k4").attr("required");
        $("#k5").attr("required");
    });
    //Kin Section End

    //Guardian Section Start
    $("#guardianMemberBlock").hide();
    $("#guardianNotMemberBlock").hide();
    $("#guardianMember").click(function(){
        $("#guardianMemberBlock").fadeIn(500);
        $("#guardianNotMemberBlock").fadeOut();
        $("#g2").removeAttr("required");
        $("#g3").removeAttr("required");
        $("#g4").removeAttr("required");
        $("#g5").removeAttr("required");
        $("#g6").removeAttr("required");
        $("#g1").attr("required");

    });
    $("#guardianNotMember").click(function(){
        $("#guardianMemberBlock").fadeOut();
        $("#guardianNotMemberBlock").fadeIn(500);
        $("#g1").removeAttr("required");
        $("#g2").attr("required");
        $("#g3").attr("required");
        $("#g4").attr("required");
        $("#g5").attr("required");
        $("#g6").attr("required");
    });
    //Guardian Section End
});