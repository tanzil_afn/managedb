/**
 * Created by afnan on 3/12/16.
 */
var path = require('path');



exports.createGet = function(req, res)
{
	res.render('CreateTable', { title: 'Home'});
};

exports.createPost = function(req, res)
{
	var databaseName = req.session.dbName;

	var data_base = require(path.resolve('Database/agdm_sqlite3.js'));

	//accesing query parameter to get the table name
	//var tableName = req.params.name.toString();

	var tableName = req.body.tableName;

	var sqlite3 = new data_base.agdm.db.sqlite3.Core;
	sqlite3.Init(
		databaseName,
		function(msg){
			console.log(msg);
			onSucc_CreateTable();
		},
		function(msg, err){
			console.log(msg);
			console.log(err);
		}
	);

	function onSucc_CreateTable()
	{

		var table = new data_base.agdm.db.sqlite3.Table(tableName, sqlite3);

		var field_array = [];

		var fields = req.body.tableData;

		fields.forEach(function(col)
		{
			//var field = new data_base.agdm.db.sqlite3.Field(col.name, col.type, col.NotNull);
			var field = new data_base.agdm.db.sqlite3.Field(col.name, col.type, [col.NotNull]);

			field_array.push(field);
		});

		table.Create(
			function(row){
				console.log("Table Created");
				res.send("Table Created");
			},
			function(err){
				console.log("Table Not Created");
				//res.send("Table Not Created !!!!");
				res.send(err);
			},
			field_array
		);
	}
	//res.send("Table Created");

		//res.redirect('SQLITEtable',{title: '', data: null, colm:"" ,tables:[]});

};