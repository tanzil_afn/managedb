/**
 * Created by afnan on 4/23/16.
 */

var path = require('path');

var tableName ;
var data_base ;
var sqlite3 ;
var table ;
var str_wh;

exports.editRowGet = function(req, res){
    var databaseName = req.session.dbName;
    var tableList = null;
    var str_sel = "*";
    str_wh = "Id = "+ req.query.Id.toString();

    data_base = require(path.resolve('Database/agdm_sqlite3.js'))

    tableName = req.query.tableName.toString();

    sqlite3 = new data_base.agdm.db.sqlite3.Core;

    sqlite3.Init(
        databaseName,
        function(msg){
            console.log(msg);
        },
        function(msg, err){
            console.log(msg);
            console.log(err);
        }
    );

    table = new data_base.agdm.db.sqlite3.Table(tableName, sqlite3);

    table.Select1(
        function(row,colms){
            res.render('editRow',{title: 'Home', data: row, colm: colms ,tables:[], table:tableName});
            //row = JSON.stringify(row);
            //res.send(row);
        },
        function(err){
            console.log(err);
            console.log("Row Selection Error");
        },
        str_sel,
        str_wh
    );

}

exports.editRowPost = function(req, res)
{
    var data = req.body;

    var str_upd="";

    for(var key in data)
    {
        str_upd += key.toString() +" = "+data[key] +" , ";
    }
    str_upd = str_upd.substring(0,str_upd.length-2);

    table.Update1(
        function(row){
            res.send("Table Updated!!");
        },
        function(err){
            res.send(err.toString());
        },
        str_upd,
        str_wh
    );

    //res.send(str_wh);
}